﻿using System.Web.Optimization;
using System.Web.UI;

namespace ECMAScript6RequireTypescript.Web
{
	public static class BundleConfig
	{
		// For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkID=303951
		public static void RegisterBundles(BundleCollection bundles)
		{
			// Order is very important for these files to work, they have explicit dependencies
			bundles.Add(new ScriptBundle("~/bundles/MsAjaxJs").Include(
					"~/Scripts/Libraries/MsAjax/MicrosoftAjax.js",
					"~/Scripts/Libraries/MsAjax/MicrosoftAjaxWebForms.js"));

			ScriptManager.ScriptResourceMapping.AddDefinition("MsAjaxBundle", new ScriptResourceDefinition
			{
				Path = "~/bundles/MsAjaxJs",
				CdnPath = "http://ajax.aspnetcdn.com/ajax/4.5/6/MsAjaxBundle.js",
				LoadSuccessExpression = "window.Sys",
				CdnSupportsSecureConnection = true
			});

			ScriptManager.ScriptResourceMapping.AddDefinition(
				"respond",
				new ScriptResourceDefinition
				{
					Path = "~/Scripts/respond.min.js",
					DebugPath = "~/Scripts/respond.js",
				});
		}
	}
}