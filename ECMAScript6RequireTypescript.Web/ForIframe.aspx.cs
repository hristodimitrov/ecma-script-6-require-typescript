﻿using System;
using ECMAScript6RequireTypescript.Web.Infrastructure.BasePage;

namespace ECMAScript6RequireTypescript.Web
{
    public partial class ForIframe : BasePage
    {
        protected override void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender,e);

            LoadScript("es6!es6Demo");
        }
    }
}