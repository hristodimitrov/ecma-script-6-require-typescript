﻿using System;
using System.Web.UI;

namespace ECMAScript6RequireTypescript.Web.Infrastructure.BasePage
{
	public class BasePage : Page
	{
		private const string REQUIRE_PATH = "/Scripts/Libraries/Require/require.js";
		private const string REQUIRE_NAME = "require";
        private const string REQUIRE_CONFIG_PATH = "/Scripts/requireConfig.js";
        private const string COMMON_NAME = "common";

		private const string INCLUDE_SCRIPT_FORMAT =
@"<script src='{0}'></script>";

		private const string REQUIRE_SCRIPT_FORMAT =
@"require(['{0}'], function () {{
	require(['{1}']);
}});
";

		//Methods for loading scripts
		protected void LoadScript(string scriptName)
		{
            var script = String.Format(REQUIRE_SCRIPT_FORMAT, REQUIRE_CONFIG_PATH, scriptName);

			ScriptManager.RegisterStartupScript(this, GetType(), scriptName, script, true);
		}

		protected void LoadScriptReference(string scriptName, string scriptUrl)
		{
			var script = String.Format(INCLUDE_SCRIPT_FORMAT, scriptUrl);

			ScriptManager.RegisterStartupScript(this, GetType(), scriptName, script, false);
		}

		//Methods loading innitial scripts
		private void LoadRequire()
		{
            LoadScriptReference(REQUIRE_NAME, REQUIRE_PATH);
		}

		private void LoadCommon()
		{
			LoadScript(COMMON_NAME);
		}



		protected virtual void Page_Load(object sender, EventArgs e)
		{
			LoadRequire();
            LoadCommon();
		}
	}
}