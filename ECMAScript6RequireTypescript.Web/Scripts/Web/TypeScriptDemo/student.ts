﻿import Person = require("iPerson");

class Student implements Person {
	fullname: string;

	constructor(public firstName, public middleName, public lastName) {
		this.fullname = firstName + " " + middleName + " " + lastName;
	}
}

//private, static, get, set, inheritance, declare 

export = Student;