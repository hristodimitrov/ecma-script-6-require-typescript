﻿function identity<T>(arg: T): T {
    return arg;
}

var myIdentity: <U>(arg: U) => U = identity;


class GenericNumber<T> {
    zeroValue: T;
    add: (x: T, y: T) => T;
}

var myGenericNumber = new GenericNumber<number>();
myGenericNumber.zeroValue = 0;
myGenericNumber.add = function (x, y) { return x + y; };