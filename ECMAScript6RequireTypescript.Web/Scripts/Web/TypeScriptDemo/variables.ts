﻿var height: number = 6;

var name: string = "Hristo";

var isTrue: boolean = true;

export enum Color { Red, Green, Blue };
var colorVarible: Color = Color.Green;

export var array: Array<string> = ["Gosho", "Tosho", "Lucho"];

var notSure: any = 4;
notSure = "maybe a string instead";
notSure = false; // okay, definitely a boolean 