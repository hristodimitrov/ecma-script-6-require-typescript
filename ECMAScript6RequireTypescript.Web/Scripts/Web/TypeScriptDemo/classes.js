﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports"], function(require, exports) {
    var Dog = (function () {
        function Dog(name, bark) {
            this.name = name;
            this.bark = bark;
        }
        return Dog;
    })();

    var Shepard = (function (_super) {
        __extends(Shepard, _super);
        function Shepard(name, bark, color) {
            this.color = color;

            _super.call(this, name, bark);
        }
        return Shepard;
    })(Dog);
});
//# sourceMappingURL=classes.js.map
