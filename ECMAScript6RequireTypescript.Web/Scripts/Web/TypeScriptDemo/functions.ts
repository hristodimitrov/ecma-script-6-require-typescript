﻿function add(x: number, y: number): number {
    return x + y;
}

function buildName(firstName: string, ...restOfName: string[]) {
    return firstName + " " + restOfName.join(" ");
}

var buildNameFun: (fname: string, ...rest: string[]) => string = buildName;

//optional ? =    lambda