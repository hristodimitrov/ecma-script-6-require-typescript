﻿define(["require", "exports"], function(require, exports) {
    var Student = (function () {
        function Student(firstName, middleName, lastName) {
            this.firstName = firstName;
            this.middleName = middleName;
            this.lastName = lastName;
            this.fullname = firstName + " " + middleName + " " + lastName;
        }
        return Student;
    })();

    
    return Student;
});
//# sourceMappingURL=student.js.map
