﻿function identity(arg) {
    return arg;
}

var myIdentity = identity;

var GenericNumber = (function () {
    function GenericNumber() {
    }
    return GenericNumber;
})();

var myGenericNumber = new GenericNumber();
myGenericNumber.zeroValue = 0;
myGenericNumber.add = function (x, y) {
    return x + y;
};
//# sourceMappingURL=generics.js.map
