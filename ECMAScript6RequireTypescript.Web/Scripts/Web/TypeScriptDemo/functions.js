﻿function add(x, y) {
    return x + y;
}

function buildName(firstName) {
    var restOfName = [];
    for (var _i = 0; _i < (arguments.length - 1); _i++) {
        restOfName[_i] = arguments[_i + 1];
    }
    return firstName + " " + restOfName.join(" ");
}

var buildNameFun = buildName;
//optional ? =    lambda
//# sourceMappingURL=functions.js.map
