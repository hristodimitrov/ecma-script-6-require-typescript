﻿require.config({
    baseUrl: "Scripts",
    paths: {
        //Libraries
        jquery: "Libraries/JQuery/jquery-1.10.2.min",
        bootstrap: "Libraries/Bootstrap/bootstrap",
        respond: "Libraries/Bootstrap/respond",
        babel: "Libraries/Babel/babel",
        es6: "Libraries/ES6/es6",
        //Common
        common: "Common/common",
        //Web Pages Scripts
        "HUNTER.homePage": "Web/Home/homePage",
        variables: "Web/TypeScriptDemo/variables",
        functions: "Web/TypeScriptDemo/functions",
        student: "Web/TypeScriptDemo/student",
        generics: "Web/TypeScriptDemo/generics",
        es6Demo: "Web/ES6/es6Demo"
    },
    shim: {
        bootstrap: {
            deps: ["jquery"]
        }
    }
});
//# sourceMappingURL=requireConfig.js.map
